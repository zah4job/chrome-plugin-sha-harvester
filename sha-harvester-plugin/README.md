# SHA harveser plugin
This plugin work in specific flavors ;)

I need to take from browser page all <a class='ng-href'> elements.
Such items have href, which sounds like 'https://somedomain/filename#sha256=abcdef'
All i need is take filename and sha256 value and build multiline string.
String will be like:
{ file = 'filename', hash = 'sha256:abcdef' }
{ file = 'filename', hash = 'sha256:abcdef' }
and so on...

# License
Free to use this in any purpose with no guaranties by author.

# Install
To install this plugin go to chrome://extension tab of your Chrome browser,
switch to developer mode and load folder with plugin.
You are awesome!