function main() {
    let ans = []
    let elements = document.querySelectorAll('a.ng-binding');
    elements.forEach((item) => {
        let href = item.attributes['ng-href'];
       
        if (href !== undefined) {
            let fullString = href.value;
            let splitted = fullString.split('#')
            let fileUrl = splitted[0]
            let sha256 = splitted[1].replaceAll('=', ':')
            splitted = fileUrl.split('/')
            let file = splitted[splitted.length - 1]
            ans.push('{file = "' + file + '", hash = "' + sha256 + '"},')
        }        
    })

    return ans;
}
main()