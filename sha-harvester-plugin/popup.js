var tabId = 0
chrome.tabs.query({active: true}, function(tabs) {
    var tab = tabs[0];
    chrome.scripting.executeScript({
        target: {tabId: tab.id, allFrames: true},
        files: ['work.js'],
    }, ()=>{});
  });


$(document).ready(()=>{
    $('#harvest').click(harvest);
})

function harvest(){
    chrome.tabs.query({active: true}, function(tabs) {
        var tab = tabs[0];
        chrome.scripting.executeScript({
            target: {tabId: tab.id, allFrames: true},
            files: ['work.js']
        }, (result)=>{
            let result_string = ''
             result[0]['result'].forEach((item) => {
                result_string += item + '\r\n'
            })
            $('#output').html('copied in clipboard items: ' + result[0]['result'].length);
            navigator.clipboard.writeText(result_string)
        });
      });
}